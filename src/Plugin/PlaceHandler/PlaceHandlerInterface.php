<?php

namespace Drupal\places\Plugin\PlaceHandler;

use Drupal\places\Entity\Place;

/**
 * Interface for place handler plugins.
 */
interface PlaceHandlerInterface {

  /**
   * Get the field definitions associated with this place handler.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *   The base field definitions.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   The base field definitions.
   */
  public function fieldDefinitions(array $base_field_definitions);

  /**
   * Act just before a place is saved.
   *
   * @param \Drupal\places\Entity\Place $place
   *   The place entity.
   */
  public function onPreSave(Place $place);

  /**
   * Act whenever anything changes.
   *
   * @param \Drupal\places\Entity\Place $place
   *   The place entity.
   * @param string $name
   *   The property that has changed.
   */
  public function onChange(Place $place, $name);

}
