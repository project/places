<?php

namespace Drupal\places\Plugin\PlaceHandler;

use Drupal\places\Entity\Place;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * The taxonomy address place handler plugin.
 *
 * @PlaceHandler(
 *   id = "taxonomy_address",
 *   label = @Translation("Taxonomy Address"),
 * )
 */
class TaxonomyAddressPlaceHandler extends SimpleAddressPlaceHandler {

  /**
   * {@inheritdoc}
   */
  public function fieldDefinitions(array $base_field_definitions) {
    $fields = parent::fieldDefinitions($base_field_definitions);

    $fields['location_tree'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Location (Tree)'))
      ->setName('location_tree')
      ->setTargetEntityTypeId('place')
      ->setTargetBundle($this->getPluginId())
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'locations' => 'locations',
        ],
        'auto_create' => TRUE,
        'auto_create_bundle' => 'locations',
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function onPreSave(Place $place) {
    parent::onPreSave($place);

    $terms = [];
    $data = json_decode($place->get('geo')->value);
    if (!$data || !isset($data->properties->adminLevels)) {
      return;
    }

    // First get the tem for the country.
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $lterms = $term_storage->loadByProperties([
      'vid' => 'locations',
      'name' => $data->properties->country,
    ]);
    if (!($term = reset($lterms))) {
      $term = $term_storage->create([
        'vid' => 'locations',
        'name' => $data->properties->country,
      ]);
      $term->save();
    }
    $terms[] = $term;

    foreach ($data->properties->adminLevels as $level) {
      $lterms = $term_storage->loadByProperties([
        'vid' => 'locations',
        'name' => $level->name,
        'parent' => $term->id(),
      ]);
      if (!($next_term = reset($lterms))) {
        $next_term = $term_storage->create([
          'vid' => 'locations',
          'name' => $level->name,
          'parent' => $term->id(),
        ]);
        $next_term->save();
      }
      $terms[] = $term = $next_term;
    }

    $place->set('location_tree', $terms);
  }

}
