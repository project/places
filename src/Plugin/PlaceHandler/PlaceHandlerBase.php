<?php

namespace Drupal\places\Plugin\PlaceHandler;

use Drupal\Core\Plugin\PluginBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\places\Entity\Place;

/**
 * The base place handler plugin.
 */
class PlaceHandlerBase extends PluginBase implements PlaceHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function fieldDefinitions(array $base_field_definitions) {
    $fields = [];

    $fields['geo'] = BundleFieldDefinition::create('geofield')
      ->setLabel('Geolocation')
      ->setName('geo')
      ->setTargetEntityTypeId('place')
      ->setTargetBundle($this->getPluginId())
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setProvider('places')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function onPreSave(Place $place) {
  }

  /**
   * {@inheritdoc}
   */
  public function onChange(Place $place, $name) {
  }

}
