<?php

namespace Drupal\places\Plugin\PlaceHandler;

use Drupal\places\Entity\Place;

/**
 * The simple address place handler plugin.
 *
 * @PlaceHandler(
 *   id = "address",
 *   label = @Translation("Simple Address"),
 * )
 */
class SimpleAddressPlaceHandler extends AddressPlaceHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function fieldDefinitions(array $base_field_definitions) {
    $fields = parent::fieldDefinitions($base_field_definitions);

    $fields['name'] = $base_field_definitions['name']
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function onPreSave(Place $place) {
    parent::onPreSave($place);

    // Include more information from the address if not label is set.
    if ($place->name->isEmpty()) {
      $label_bits = array_filter([
        $place->address->locality,
        $place->address->administrative_area,
        $place->address->country_code,
      ]);
      $place->set('name', implode(', ', $label_bits));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onChange(Place $place, $name) {
    parent::onChange($place, $name);

    if ($name == 'address') {
      $label_bits = array_filter([
        $place->address->locality,
        $place->address->administrative_area,
        $place->address->country_code,
      ]);
      $place->set('name', implode(', ', $label_bits));
    }
  }

}
