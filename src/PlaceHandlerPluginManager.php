<?php

namespace Drupal\places;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The Place Handler Plugin Manager.
 */
class PlaceHandlerPluginManager extends DefaultPluginManager {

  /**
   * Constructs a PlaceHandlerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/PlaceHandler',
      $namespaces,
      $module_handler,
      'Drupal\places\Plugin\PlaceHandler\PlaceHandlerInterface',
      'Drupal\places\Annotation\PlaceHandler'
    );
    $this->alterInfo('place_handler');
    $this->setCacheBackend($cache_backend, 'place_handler');
  }

}
