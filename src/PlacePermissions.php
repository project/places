<?php

namespace Drupal\places;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The jobs place permission handler.
 *
 * @package Drupal\places
 */
class PlacePermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The place plugin manager.
   *
   * @var \Drupal\places\PlaceHandlerPluginManager|null
   */
  protected $handlerManager = NULL;

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.places.place_handler'),
      $container->get('string_translation')
    );
  }

  /**
   * PlacePermissions constructor.
   *
   * @param \Drupal\places\PlaceHandlerPluginManager $handler_manager
   *   The place plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(PlaceHandlerPluginManager $handler_manager, TranslationInterface $string_translation) {
    $this->handlerManager = $handler_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Get permissions for place entities.
   *
   * @return array
   *   Permissions array for place entities.
   */
  public function permissions() {
    $permissions = [];

    foreach ($this->handlerManager->getDefinitions() as $id => $info) {
      $params = [
        '%type' => $info['label'],
      ];

      $permissions += [
        "create $id place" => [
          'title' => $this->t('Create new %type places', $params),
        ],
        "view own $id place" => [
          'title' => $this->t('View own %type places', $params),
        ],
        "view any $id place" => [
          'title' => $this->t('View any %type places', $params),
        ],
        "edit own $id place" => [
          'title' => $this->t('Edit own %type places', $params),
        ],
        "edit any $id place" => [
          'title' => $this->t('Edit any %type places', $params),
        ],
        "delete own $id place" => [
          'title' => $this->t('Delete own %type places', $params),
        ],
        "delete any $id place" => [
          'title' => $this->t('Delete any %type places', $params),
        ],
      ];
    }

    return $permissions;
  }

}
