<?php

namespace Drupal\places\Entity;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Ticket entities.
 */
class PlaceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Place::$geo.
    $data['place__geo']['table'] = [
      'entity_type' => 'place',
      'join' => [
        'place' => [
          'table' => 'place__geo',
          'left_field' => 'id',
          'field' => 'entity_id',
          'extra' => [
            [
              'field' => 'deleted',
              'value' => 0,
              'numeric' => TRUE,
            ],
          ],
        ],
      ],
    ];
    $data['place__geo']['geo'] = [
      'group' => new TranslatableMarkup('Place'),
      'title' => new TranslatableMarkup('Geolocation'),
      'help' => new TranslatableMarkup('Geolocation for the place.'),
      'field' => [
        'id' => 'field',
        'field_name' => 'geo',
        'entity_type' => 'place',
        'real field' => 'geo_value',
        'additional fields' => [
          'delta',
          'langcode',
          'bundle',
          'geo_value',
          'geo_geo_type',
          'geo_lat',
          'geo_lon',
          'geo_left',
          'geo_top',
          'geo_right',
          'geo_bottom',
          'geo_geohash',
        ],
        'element type' => 'div',
        'is revision' => FALSE,
        'click sortable' => TRUE,
        'table' => 'contacts_job__temp_geo',
      ],
    ];

    $data['place__geo']['geo_value'] = [
      'group' => new TranslatableMarkup('Place'),
      'title' => new TranslatableMarkup('Geolocation'),
      'help' => new TranslatableMarkup('Whether this ticket is a team ticket.'),
    ];
    $data['place__address']['table'] = [
      'join' => [
        'place' => [
          'table' => 'place__address',
          'left_field' => 'id',
          'field' => 'entity_id',
          'extra' => [
            [
              'field' => 'deleted',
              'value' => 0,
              'numeric' => TRUE,
            ],
          ],
        ],
      ],
    ];
    $data['place__address']['address'] = [
      'group' => new TranslatableMarkup('Place'),
      'title' => 'Address',
      'help' => 'Address of the Place entity',
      'field' =>
        [
          'table' => 'place__address',
          'id' => 'field',
          'field_name' => 'address',
          'entity_type' => 'place',
          'real field' => 'address_langcode',
          'additional fields' =>
            [
              0 => 'delta',
              1 => 'langcode',
              2 => 'bundle',
              3 => 'address_langcode',
              4 => 'address_country_code',
              5 => 'address_administrative_area',
              6 => 'address_locality',
              7 => 'address_dependent_locality',
              8 => 'address_postal_code',
              9 => 'address_sorting_code',
              10 => 'address_address_line1',
              11 => 'address_address_line2',
              12 => 'address_organization',
              13 => 'address_given_name',
              14 => 'address_additional_name',
              15 => 'address_family_name',
            ],
          'element type' => 'div',
          'is revision' => FALSE,
          'click sortable' => TRUE,
        ],
    ];
    $data['place__address']['address_country_code'] = [
      'group' => new TranslatableMarkup('Place'),
      'title' => 'Address (Country Code)',
      'help' => 'Country code of the Place address field',
      'argument' => [
        'field' => 'address_country_code',
        'table' => 'place__address',
        'id' => 'string',
        'additional fields' => [
          0 => 'bundle',
          1 => 'deleted',
          2 => 'entity_id',
          3 => 'revision_id',
          4 => 'langcode',
          5 => 'delta',
          6 => 'address_langcode',
          7 => 'address_country_code',
          8 => 'address_administrative_area',
          9 => 'address_locality',
          10 => 'address_dependent_locality',
          11 => 'address_postal_code',
          12 => 'address_sorting_code',
          13 => 'address_address_line1',
          14 => 'address_address_line2',
          15 => 'address_organization',
          16 => 'address_given_name',
          17 => 'address_additional_name',
          18 => 'address_family_name',
        ],
        'field_name' => 'address',
        'entity_type' => 'place',
      ],
      'field' => [
        'id' => 'country',
        'field_name' => 'address',
        'property' => 'country_code',
      ],
    ];

    return $data;
  }

}
